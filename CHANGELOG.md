# CHANGELOG



## v0.1.0 (2024-03-29)

### Feature

* feat(test): add new test module ([`1fa72d4`](https://gitlab.com/yjr.1589/my-package-2/-/commit/1fa72d49778df8ba4643ccdd071717134590c5fc))

* feat(test): add new test module ([`2401e94`](https://gitlab.com/yjr.1589/my-package-2/-/commit/2401e9419d87aba207f619c2a3cce09e4ba69a14))


## v0.0.0 (2024-03-29)

### Unknown

* Modificando el archivo gitlab-ci.yml ([`0857245`](https://gitlab.com/yjr.1589/my-package-2/-/commit/085724562ab27713f3b89640f2ce40439391934f))

* Actualizando el archivo pyproject.toml ([`c982f94`](https://gitlab.com/yjr.1589/my-package-2/-/commit/c982f94a9834a70b0e89660b63284ed0359ee3e3))

* Agregando dependencias usando poetry ([`e626693`](https://gitlab.com/yjr.1589/my-package-2/-/commit/e626693ad1e7912804021b8f2d329447f13dfc7e))

* &#34;Creando archivo pyproject.toml&#34; ([`075b425`](https://gitlab.com/yjr.1589/my-package-2/-/commit/075b425c71bee9fd13b6a77c6a45f5d86e4a94d5))

* Update .gitlab-ci.yml file ([`bffd09a`](https://gitlab.com/yjr.1589/my-package-2/-/commit/bffd09afa581ac254a4949a7fcba56a53cc1641a))

* Add first pipeline ([`90ed5b6`](https://gitlab.com/yjr.1589/my-package-2/-/commit/90ed5b6252ca19cd93830478c5f447808cd6ef20))

* Initial commit ([`ba9dbf7`](https://gitlab.com/yjr.1589/my-package-2/-/commit/ba9dbf77f68a0ed9eb2fad7aa955c4c69a9a6d8e))
